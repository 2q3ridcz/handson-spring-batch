# handson-spring-batch

[Spring Boot バッチサービスの作成 - 公式サンプルコード](https://spring.pleiades.io/guides/gs/batch-processing/) で勉強する。

上記サイトだけでは handson を完結できなかった。下記 GitHub のソースコードも確認しながら進めた。(後から振り返ると、必要なことは全て書いてあるのだが。)

[GitHub - spring-guides/gs-batch-processing: Creating a Batch Service :: Learn how to create a basic batch-driven solution.](https://github.com/spring-guides/gs-batch-processing)

## VSCode での Project 作成

Java 11 で作成した。

1. Ctrl + Shift + P でコマンドパレットを開く。「Java: Create Java Project」を選択。   
    ![create-spring-batch-project_01.png](./docs/img/create-spring-batch-project/create-spring-batch-project_01.png)
2. 「Spring Boot」を選択。  
    ![create-spring-batch-project_02.png](./docs/img/create-spring-batch-project/create-spring-batch-project_02.png)
3. 「Maven Project」を選択。
    ![create-spring-batch-project_03.png](./docs/img/create-spring-batch-project/create-spring-batch-project_03.png)
4. 「2.7.5」を選択。(「「SNAPSHOT」や「M1」などが付いていない最新バージョン」)  
    ![create-spring-batch-project_04.png](./docs/img/create-spring-batch-project/create-spring-batch-project_04.png)
5. 「Java」を選択。  
    ![create-spring-batch-project_05.png](./docs/img/create-spring-batch-project/create-spring-batch-project_05.png)
6. 今回は「com.example」のまま Enter。  
    ![create-spring-batch-project_06.png](./docs/img/create-spring-batch-project/create-spring-batch-project_06.png)
7. 「batchprocessing」を入力して Enter。  
    ![create-spring-batch-project_07.png](./docs/img/create-spring-batch-project/create-spring-batch-project_07.png)
8. 「Jar」を選択。  
    ![create-spring-batch-project_08.png](./docs/img/create-spring-batch-project/create-spring-batch-project_08.png)
9. 「11」を選択。  
    ![create-spring-batch-project_09.png](./docs/img/create-spring-batch-project/create-spring-batch-project_09.png)
10. 「Spring Batch」と「HyperSQL Database」を選択して Enter。  
    ![create-spring-batch-project_10.png](./docs/img/create-spring-batch-project/create-spring-batch-project_10.png)
11. 保存先を選択して「Generate into this folder」を押下。  
    ![create-spring-batch-project_11.png](./docs/img/create-spring-batch-project/create-spring-batch-project_11.png)
12. 「Open」を押下すると Spring Boot プロジェクトの 新しい VSCode Window が開く。
    ![create-spring-batch-project_12.png](./docs/img/create-spring-batch-project/create-spring-batch-project_12.png)

## 実装

冒頭のサイトのとおりに進めれば、問題なく動かせた。  
ソースを作成後に下記コマンドでコンパイル＆起動。

```
./mvnw clean package
java -jar target/batchprocessing-0.0.1-SNAPSHOT.jar
```
